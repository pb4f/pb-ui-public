//Vue3
//import  { createApp } from 'vue'
import  Vue from 'vue'
import App from './App.vue'
import i18n from './i18n'
import store from './store'

Vue.config.productionTip = false

//const appEl = document.querySelector('#app')
//Vue3
/*const app = createApp(App, { ...appEl.dataset })
app.use(i18n)
app.use(store)
app.mount('#app')*/

var app = new Vue({
  i18n,
  store,
  render: h => h(App)
})
app.$mount('#app')
