import Vue from 'vue'
import Vuex from 'vuex'
import pbapi from './modules/pbapi'

const debug = process.env.NODE_ENV !== 'production'

Vue.use(Vuex)

export default new Vuex.Store({
    namespaced: true,
    modules : {
        pbapi
    },
    getters : {
        test : () => {
            return "ok";
        }
    },
    strict: debug
})