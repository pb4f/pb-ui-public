module.exports = {
  publicPath: './',
  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: true
    }
  },
  pages: {
    'index': {
      entry: 'src/main.js',
      template: 'public/index.html',
      title: 'pbui',
      chunks: ['chunk-common', 'chunk-vendors', 'index']
    },
    'alternate': {
      entry: 'src/main.js',
      template: 'public/alternate.html',
      title: 'alternateui',
      chunks: ['chunk-common', 'chunk-vendors', 'index']
    }
  }
}
